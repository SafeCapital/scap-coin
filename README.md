SafeCapital Core integration/staging repository
=====================================

SafeCapital is a platform for unhindered decentralized finance. Whereas traditional investing relies on centralized firms, services, and brokers (along with their fees, rules, and roadblocks), SafeCapital's DeFi platform gives you complete freedom to pursue the best investments for you. 

SafeCapital is the FIRST Masternode Coin to Locks collateral for a period of one year!

Masternodes will never look the same again.
SafeCapital locks masternode collateral for a year, keeping value in the system, which, in turn, creates better financial opportunities for ecosystem participants. 

More information at [SafeCapital.io](https://www.SafeCapital.io)

### Coin Specs

|   |   |
|---|---|
|Type|POS + Masternode|
|Algo|Quark|
|Block Time|60 Seconds|
|Maturity|100 Confirmations|
|Difficulty Retargeting|Every Block|
|Masternode Collateral|1,000.00019 SCAP|
|Masternode Collateral Locked|YES|
|Masternode Collateral Time Locked|One Year|
|P2P port|47002|
|RPC port|47003|
|Max Coin Supply|6,600,000 SCAP|

*View Coin Distribution on [SafeCapital.io](https://www.safecapital.io/#token-distribution)

### Reward Distribution


|Block no|Reward/block|Staking|Masternodes|
|---|---|---|---|
|1 - 10800|0.20|90%|10%|
|10801 - 21600|0.30|90%|10%|
|21601 - 32400|0.40|90%|10%|
|32401 - 43200|0.50|90%|10%|
|43201 - 54000|0.60|90%|10%|
|54001 - 64800|0.70|90%|10%|
|64801 - 75600|0.80|90%|10%|
|75601 - 86400|0.90|90%|10%|
|86401 - 97200|1.00|90%|10%|
|97201 - 108000|1.10|90%|10%|
|108001 - 118800|1.20|90%|10%|
|118801 - 172800|1.30|90%|10%|
|172801 - 259200|1.40|90%|10%|
|259201 - 345600|1.50|90%|10%|
|345601 - 432000|1.60|90%|10%|
|432001 - 518400|1.70|90%|10%|
|518401 - 604800|1.80|90%|10%|
|604801 - 691200|1.90|90%|10%|
|691201 - 777600|2.00|90%|10%|
|777601 - 864000|2.10|90%|10%|
|864001 - 950400|2.20|90%|10%|
|950401 - 1036800|2.30|90%|10%|
|1036801 - 1123200|2.40|90%|10%|
|1123201 - 1209600|2.50|90%|10%|
|1209601 - 1296000|2.60|90%|10%|
|1296001 - 1382400|2.70|90%|10%|
|1382401 - 1468800|2.80|90%|10%|
|1468801 - 1555200|2.90|90%|10%|
|1555201 - 1576800|3.00|90%|10%|
|*After 3 years, each year, the reward will be reduced with 40%.|
